#!/usr/bin/python3
# Configuration mode is entered and a srx configuration from a file
# is issued as a list of configuration set and/or delete commands written in the file
# By Per Dahlstroem 2019

import netmiko

myConnection = netmiko.ConnectHandler(ip='192.168.2.1',
                                      device_type='juniper',
                                      username='root',
                                      password='Rootpass',
                                      verbose=True,
                                      encoding='UTF-8')
myConnection.config_mode()
print(myConnection.send_config_from_file("SRXconfigurations/srxConfigSetCommandsV04.set",
                                           exit_config_mode=False))
# "/home/per/programmes/SRXconfigurations/srxConfigSetCommandsV04.set"
print myConnection.commit(and_quit=True) # Exit configuration mode
myConnection.exit_config_mode()
myConnection.disconnect()