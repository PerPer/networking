
junosFileFetchGUIV02.py

1    # Python 2.7 on Linux 
2    import Tkinter 
3    import ScrolledText as tkst # Scrolled text widget for large text display 
4    import paramiko, time, os 
5     
6    class FileFetchGUI(): 
7     
8        def __init__(self, sshComm): 
9            self.sshCommunication = sshComm 
10    
11           self.master = Tkinter.Tk(  )            # Create master widget object 
12           self.junosIPVar = Tkinter.StringVar()   # Prepare a variable to hold 
13    
14           self.toggle = 0 
15    
16           self.junosIPVar     = '10.10.1.1' 
17           self.junosUser      = 'root' 
18           self.junosPasswrd   = 'rootpass12' 
19           self.junosPath      = '//' 
20           self.junosFileName  = '???' 
21           self.destIP         = '10.10.1.9' 
22           self.dstUserName    = 'per' 
23           self.dstPasswrd     = 'perpass' 
24           self.dstPath        = '/home/per' 
25           self.dstFileName    = 'junos.conf.zg' 
26    
27           Tkinter.Label(self.master, text="Junos Network device: ").   \ 
28                                           grid(row=0, sticky="E") 
29           Tkinter.Label(self.master, text="       ").   \ 
30                                           grid(row=0, column=4, sticky="E") # Spa 
31           Tkinter.Label(self.master, text="IP:").\ 
32                                           grid(row=1, sticky="W") 
33           Tkinter.Label(self.master, text="User name:").\ 
34                                           grid(row=2, sticky="W") 
35           Tkinter.Label(self.master, text="Password:").\ 
36                                           grid(row=3, sticky="W") 
37           Tkinter.Label(self.master, text="File path:").\ 
38                                           grid(row=4, sticky="W") 
39           Tkinter.Label(self.master, text="File name:").\ 
40                                           grid(row=5, sticky="W") 
41           Tkinter.Label(self.master, text="Receiving (this) Network device: ").\ 
42                                           grid(row=6, sticky="E") 
43           Tkinter.Label(self.master, text="IP:").\ 
44                                           grid(row=7, sticky="W") 
45           Tkinter.Label(self.master, text="User name:").\ 
46                                           grid(row=8, sticky="W") 
47           Tkinter.Label(self.master, text="Password:").\ 
48                                           grid(row=9, sticky="W") 
49           Tkinter.Label(self.master, text="File path:").\ 
50                                           grid(row=10, sticky="W") 
51           Tkinter.Label(self.master, text="File name:").\ 
52                                           grid(row=11, sticky="W") 
53           Tkinter.Label(self.master, text="").\ 
54                                           grid(row=12, sticky="W") # Spacer label 
55           Tkinter.Label(self.master, text="").\ 
56                                           grid(row=14, sticky="W") # Spacer label 
57           # Create entry boxes 
58           self.entry1 = Tkinter.Entry(self.master) 
59           self.entry2 = Tkinter.Entry(self.master) 
60           self.entry3 = Tkinter.Entry(self.master) 
61           self.entry4 = Tkinter.Entry(self.master) 
62           self.entry5 = Tkinter.Entry(self.master) 
63           self.entry6  = Tkinter.Entry(self.master) 
64           self.entry7  = Tkinter.Entry(self.master) 
65           self.entry8  = Tkinter.Entry(self.master) 
66           self.entry9  = Tkinter.Entry(self.master) 
67           self.entry10 = Tkinter.Entry(self.master) 
68           # Place entry boxes in the grid 
69           self.entry1.grid(row=1, column=1) 
70           self.entry2.grid(row=2, column=1) 
71           self.entry3.grid(row=3, column=1) 
72           self.entry4.grid(row=4, column=1) 
73           self.entry5.grid(row=5, column=1) 
74           self.entry6.grid(row=7, column=1) 
75           self.entry7.grid(row=8, column=1) 
76           self.entry8.grid(row=9, column=1) 
77           self.entry9.grid(row=10, column=1) 
78           self.entry10.grid(row=11, column=1) 
79           # Insert a default text in entry boxes 
80    
81           self.entry1.insert(0, self.junosIPVar) 
82           self.entry2.insert(0, self.junosUser) 
83           self.entry3.insert(0, self.junosPasswrd) 
84           self.entry4.insert(0, self.junosPath) 
85           self.entry5.insert(0, self.junosFileName) 
86           self.entry6.insert(0, self.destIP) 
87           self.entry7.insert(0, self.dstUserName) 
88           self.entry8.insert(0, self.dstPasswrd) 
89           self.entry9.insert(0, self.dstPath) 
90           self.entry10.insert(0, self.dstFileName) 
91    
92           # http://effbot.org/Tkinterbook/entry.htm 
93           # Create buttons 
94           self.listEntriesButton = Tkinter.Button(self.master, 
95                                   text = "List entries", 
96                                   command = self.listEntriesButtonAction) 
97    
98           self.clearEntriesButton = Tkinter.Button(self.master, 
99                                   text = "Clear entries", 
100                                  command = self.clearEntriesButtonAction) 
101   
102          self.fetchFileButton    = Tkinter.Button(self.master, 
103                                  text = "Fetch file", 
104                                  command = self.fetchFileButtonAction) 
105   
106          self.getIPButton    = Tkinter.Button(self.master, 
107                                  text = "Get IP", 
108                                  command = self.getIPButtonAction) 
109   
110          self.quitButton         = Tkinter.Button(self.master, 
111                                  text='Quit', 
112                                  command=self.master.destroy) 
113          # Place buttons in the grid 
114          self.quitButton.grid(row=15, column=2) 
115          self.quitButton.config(relief="sunken") 
116          self.fetchFileButton.grid(row=1, column=2) 
117          self.fetchFileButton.config(relief="sunken") 
118          self.getIPButton.grid(row=7,column=2) 
119          self.listEntriesButton.grid(row=13, column=0) 
120          self.clearEntriesButton.grid(row=14, column=0) 
121          # Create info scrollable output text field 
122          self.infoTextField = tkst.ScrolledText(self.master, undo=True, 
123                                    # wrap text at full words only 
124                                      wrap   = 'word', 
125                                      width  = 60,      # characters 
126                                      height = 10,      # text lines 
127                                      # background color of edit area 
128                                      bg='grey' ) 
129   
130          self.infoTextField.grid(row=13, column=1, columnspan=2, sticky="W") 
131   
132          self.master.mainloop() 
133   
134      def listEntriesButtonAction(self): 
135          junoIP          = str(self.entry1.get()) 
136          junoUser        = str(self.entry2.get()) 
137          junoPasswrd     = str(self.entry3.get()) 
138          junoPath        = str(self.entry4.get()) 
139          junoFileName    = str(self.entry5.get()) 
140          destIP          = str(self.entry6.get()) 
141          destUserName    = str(self.entry7.get()) 
142          destPasswrd     = str(self.entry8.get()) 
143          destPath        = str(self.entry9.get()) 
144          destFileName    = str(self.entry10.get()) 
145   
146          self.infoTextField.insert('insert', junoIP + "\n" + junoUser + "\n" + 
147                                    junoPasswrd + "\n" + junoFileName + "\n" + 
148                                    destIP + "\n" + destUserName + "\n" + 
149                                    destIP + "\n" + destPasswrd + "\n" + 
150                                    destPath + "\n" + destFileName + "\n") 
151   
152      def clearEntriesButtonAction(self): 
153              junosIP = str(self.entry1.get()) 
154              self.infoTextField.delete('0.0', 'end') 
155              print junosIP 
156   
157      def getIPButtonAction(self): 
158          junosIP = str(self.entry6.get()) 
159          self.infoTextField.insert('insert', junosIP + "\n") 
160          print junosIP 
161   
162   
163      def fetchFileButtonAction(self): 
164          junoIP          = str(self.entry1.get()) 
165          junoUser        = str(self.entry2.get()) 
166          junoPasswrd     = str(self.entry3.get()) 
167          junoPath        = str(self.entry4.get()) 
168          junoFileName    = str(self.entry5.get()) 
169          destIP          = str(self.entry6.get()) 
170          destUserName    = str(self.entry7.get()) 
171          destPasswrd     = str(self.entry8.get()) 
172          destPath        = str(self.entry9.get()) 
173          destFileName    = str(self.entry10.get()) 
174          self.sshCommunication.fetchConfiguration(self, junoIP, junoUser, junoPasswrd, junoPath, 
175                                     destPath, destUserName, destIP, destPasswrd) 
176          if self.toggle == 0: 
177              self.entry1.delete(0, 'end') # Delete text from entry field 
178              self.toggle = 1 
179          else: 
180              self.entry1.delete(0, 'end') # Delete text from entry field 
181              self.entry1.insert(0, 'Button pressed') 
182              self.toggle = 0 
183   
184  class SshCommunication(): 
185   
186      def __init__(self): 
187          print 'sshCommunication object created' 
188   
189   
190      def fetchConfiguration(self, GUI, junosIP, userName, password, junosConfigPath, 
191                             hostConfigPath, hostUserName, hostIP, hostPassword): 
192          try: 
193              GUI.infoTextField.insert('insert', "Please wait. I am working on it." + "\n") 
194              self.sshClient = paramiko.SSHClient() 
195              self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy()) 
196              print 'Trying to connect to 10.10.1.1' 
197              self.sshClient.connect('10.10.1.1', username='root', password='rootpass12') 
198   
199              self.channel = self.sshClient.invoke_shell() 
200              print 'SRX shell invoked' 
201              time.sleep(1) 
202   
203              os.system('clear') 
204   
205              routerOutput = self.channel.recv(1000)  # Read router replies - empty buffer 
206   
207              self.config = '/config/juniper.conf.gz' 
208              self.configCopy = '//home/per/juniper4.conf.gz' 
209   
210              self.channel.send('scp ' + self.config + ' per@10.10.1.9:' + self.configCopy + '\n') 
211              time.sleep(1) 
212   
213              self.channel.send('perpass\n') 
214              time.sleep(1) 
215   
216              output = self.channel.recv(1000) 
217              print output 
218              self.channel.close() 
219              GUI.infoTextField.insert('insert', "Hi from SshCommunication" + "\n") 
220              GUI.infoTextField.insert('insert', output ) 
221   
222          except Exception as  ex: 
223              print 'Something went wrong.' 
224              print ex 
225   
226  class JunosConfig(): 
227   
228      def __init__(self): 
229          try: 
230              self.sshCommunication = SshCommunication() 
231              FileFetchGUI(self.sshCommunication) 
232          except Exception as  ex: 
233              print 'Something went wrong:' 
234              print ex 
235   
236  def main(): 
237      JunosConfig() 
238   
239  main() 
240  

