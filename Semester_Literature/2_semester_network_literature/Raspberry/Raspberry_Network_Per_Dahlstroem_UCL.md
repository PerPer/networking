---
title: 'Raspberry install and use Network manager'
subtitle: 'Linux'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Raspberry Networking'
---

# Audience 2021

* Students at the two year IT Technology education at UCL in Odense Denmark.  
* Anyone who wants to configure the ethernet interfaces on their Raspberry by using the Network Manager programme.  

---

# Purpose

* The purpose of this document is to show how to install and work with the Network Manager to configure  ethernet network settings on Rapsberry Pi.  
* The document is a step by step guide that should be followed from top to bottom.
* It is explained what a Network Manager Connection Profile is and some configuration examples are shown along the way.  

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

The following network is and example network where PC1, PC2 PC3 and PC4 could be Raspberries.  

![Semantic description of image](Rpi_network_images/Rpi_Generic_network.JPG "")  

---

# Versions used

* Raspbian version

The descriptions in this document are run on a raspbian version:

pi@raspberry:~ $ `cat /etc/os-release`  
PRETTY_NAME="Debian GNU/Linux 9 (stretch)"  
NAME="Debian GNU/Linux"  
VERSION_ID="9"  
VERSION="9 (stretch)"  
VERSION_CODENAME=stretch  
ID=debian  

---

# Different ways to configuring network interfaces on Raspberry

Raspberry is preconfigured (2021) to read the /etc/dhcpcd.conf network settings configuration file and configure network adapters accordingly.

It is the dhcpcd deamon or program who executes and does the configuration.  

The name dhcpcd could mislead to beleieve that it is only for dhcp, but the dhcpcd.conf  configuration is both used to configure dhcp and also to set static IPs for interfaces.  

Using the dhcpcd and dhcpcd.conf is fine and works.

But a more powerfull and maybe convenient way to set network parameters is using the Network Manager programme, either in its Command Line Interface (NMCLI) or graphically.  

Here Both the Graphical Interface and the NMCLI will be installed but only usage examples for the GUI are shown.  

---

# Kill the dhcpcd deamon

In order to not have ambiguty settings of networking interfaces, the dhcpcd program should be killed if the network Manager is used. First find the dhcpcd deamon Linux process ID:

pi@raspberry:/etc $ `ps -A | grep dhcp`  
601 ?  00:00:00 dhcpcd  

Then kill the dhcpcd deamon process:

pi@raspberry:/etc $ `sudo kill 601`

---  

# Check the file interfaces and kill dhcli dhcp client

Network Manager will ONLY handle interfaces NOT declared or configured in /etc/network/interfaces file.

Check the settings in /etc/network/interfaces

pi@raspberry:/etc $ `nano /etc/network/interfaces` 

In this case there are no settings in the file and Network Manager will thus be able to do all the setting for interfaces on this device.

There should not be any lines with text containing interface names like eth0.

![Semantic description of image](Rpi_network_images/Rpi_etc_network_interfaces_file.JPG "")  

Check if dhclient is running as this deamon uses the /etc/network/interfaces settings:

pi@raspberry:/etc $ `ps -A | grep dhcli`

Kill dhclient if it is running. Not sure if dhclient affects Network Manager as there are no settings in /etc/network/interfaces, but if dhclient is dead it will not make trouble.

---

# Install network manager

The Raspberry dosnt come bundeled with Network manager. Its is thus not listed in the Preferences.  

![Semantic description of image](Rpi_network_images/Rpi_no_networkmanager_shown.JPG "")  

## Install the Network Manager:

    $ sudo apt-get install network-manager  

    $ sudo apt install network-manager network-manager-gnome  

The Network manager will now show up in the Preferences menu as Network Connections:

![Semantic description of image](Rpi_network_images/Rpi_with_networkmanager_shown.JPG "Generic network diagram")  

Here in this example dhcpcd was NOT killed beforehand and thus populated the Network Connections with configurations:

![Semantic description of image](Rpi_network_images/Rpi_NM_GUI_network_connections.JPG "")  

Kill dhcpcd as described above.

Bring eth0 down and then up again and the (strange?) eth0 setting is gone.

![Semantic description of image](Rpi_network_images/Rpi_dhcpcd_killed.JPG "") 

Edit the Wired connection 1 and give the Wired connection 1 a better and descriptive name as shown here:

![Semantic description of image](Rpi_network_images/Rpi_edit_wired_connection_1.JPG "") 

What is actually being edited here above is a connection profile for eth0.  

A connection profile is a configuration for an interface.  

Alle intefaces like eth0 can have many different profiles i.e. configurations, that can be switched among. This is illustrated shortly. Only one profile can be chosen and thus active at a given time. 

Reboot Linux to make the changes take effect and note how the Network manager icon now apears in the upper status bar as a socket-wire icon.  

Alternatively only do a restart of the network-manager to make the changes take effect:

$ `/etc/init.d/network-manager restart`

After a reboot check if dhcpcd is again running and active.

![Semantic description of image](Rpi_network_images/Rpi_is_dhcpcd_running.JPG "") 

And it is running and active. Kill it permanently if it is not going to be used.

# Kill dhcpcd permanently.

![Semantic description of image](Rpi_network_images/Rpi_dhcpcd_killed_permanently.JPG "") 

Reboot to make the changes take effect.

After a reboot check again if dhcpcd is running. And here it did not start again.

![Semantic description of image](Rpi_network_images/Rpi_dhcpcd_killed_reboot.JPG "")

Left click the socket-wire icon in the top the Network Manager shows what connection profile or connection profiles are active in cthe case of multiple interfaces.

# Create a new connection profile

Right click and edit to make a new extra connection profile for eth0. This time creating a DHCP connection profile for eth0:

![Semantic description of image](Rpi_network_images/Rpi_edit_wired_DHCP.JPG "")

The desired connection profile for eth0 can now be chosen from the top socket-wire icon menu. Here was switched to the eth0_PDA_DHCP dhcp profile for eth0:

![Semantic description of image](Rpi_network_images/Rpi_NM_switch_connection.JPG "")

---

# Linux with two or more interfaces

If a second interface card is attach to the Raspberry, e.g. virtually in e.g. VM Ware Workstation it will show up as e.g. eth1 and so forthe for a third.  

The existing conection profiles will be available for this new interfaces card eth1. Choose a connection profile that suits or create a new one.  

As in this case here both interfaces eth0 and eth1 are running on the same type of hardware, it is not easy see which one is eth0 amd which is eth1. :-( And the eth0 and eth1 names that were given to the profiles are actually now missleading.)

![Semantic description of image](Rpi_network_images/Rpi_two_interfaces.JPG "")

## What interface is configured with what connection?

To explicitly see what has been configured for each interface, right click the top Network Manager symbol and select Connection Information. The naming for eth0 and eth1 can be wrong as it is set by the user.  
Luckily in the Interface line Linux finally shows the system name e.g. (eth1) for this interface. See below.  

![Semantic description of image](Rpi_network_images/Rpi_connection_Information.JPG "")

Here the Connection Information for eth1 is chosen but note that eth0 is also displayed in the lefthand neighbour tab.

![Semantic description of image](Rpi_network_images/Rpi_connection_Information_eth1.JPG "")

It is NOT shown here if the settings are dynamic i.e. dhcp or static. See above to reach this information.  

---

# Network Manager profiles

NetworkManager keeps connection information on known individual networks in configuration files called profiles. Those are stored at /etc/NetworkManager/system-connections/.  

For options or settings in these files refer to the manpage on nm-settings:

* man nm-settings or online.  

Connections or profiles can be edited using a text editor in the profiles or the nm-connection-editor or the nmcli.

Sources:  
https://wiki.debian.org/NetworkManager


# Make Network Manager able to configure WIFI

When dhcpcd is disablede the Raspberry(?) OS will block the WIFI wlan interface. I suppose it is because dhcpcd used to configure the wlan radio WIFI interface, and as dhcpcd is removed it no longer does this.  
To get Network Manager to control and configure the wlan radio WIFI interface, this has to be unblocked.  
The command or programme `rfkil`, meaning radio frequency kill, will do this:

`$ rfkill unblock wlan`

Here a sequence of rfkill commands to unblock the wlan rf interface and to block bluetooth are shown:

![Semantic description of image](Rpi_network_images/rfkill_unblock_wlan.jpg "")

`rfkill`  wil list the state of the networking devices alisa interfaces on the Linux box.

An interface can be:  
* software blocked or unblocked. The interface can be blocked or unblocked by the `rfkill` command.
* hardware blocked. A switch button on the devuice is blocking the or unblocking the interface.  

## Nagging message  

The OS will tell that `No wireless LAN interface found` even this is not true. I suppose it is because dhcpcd is consulted by OS and not found(?). 

![Semantic description of image](Rpi_network_images/wlan_not_found.JPG "")  

The WIFI interface will be working despite the message.  

## Listing wlans

After unblocking the wlan interface the WIFI will discover the reachable WIFI networks and list the SSIDs. Please be pacient, Raspberry needs a minute to scan for SSIDs.

![Semantic description of image](Rpi_network_images/Discovering_WIFI_APs.JPG "")

Here an iPhone network was chosen.

## Configure the wlan

From the Edit Connections... menu the Network Connection window can be reached:

![Semantic description of image](Rpi_network_images/Rpi_connection_Information.JPG "")

Now the interface profiles can be configured in the Network Connection window.

Here a profile was named iPhone as the profile i.e. Linux configuration for the iPhone hot spot network:

![Semantic description of image](Rpi_network_images/Network_Connections.JPG "")


## Configure interfaces in nmcli in command prompt

The network manager can be operated from the commandprompt in the nmcli programme.  

Please go to this document and see the nmcli section.

[https://gitlab.com/PerPer/networking/-/blob/master/Troubleshooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md](https://gitlab.com/PerPer/networking/-/blob/master/Troubleshooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md)


# Netplan

TBD

If you like YAML. :-)

