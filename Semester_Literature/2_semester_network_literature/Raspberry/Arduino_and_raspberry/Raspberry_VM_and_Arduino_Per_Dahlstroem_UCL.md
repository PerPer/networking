---
title: 'Network troubleshooting tools'
subtitle: 'Linux and Windows'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Network troubleshooting tools'
---

# Audience

Students at the two year IT Technology education at UCL in Odense Denmark.

---

# Purpose

The purpose of this document is to provide students with a short hand enrty level guide to working with arduino from a Raspberry VM on VMware Workstation.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

This generic network diagram serves as the foundation for describing network communication and includes the Arduino IoT device.

The network consists of three routes R1, R2 and R3. R1 and R2 are interconnected through network 10.10.10.0/24. R2 and R3 are interconnected through network 10.56.16.0/22. This latter network is the UCL school network.


![Semantic description of image](Generic_Network_PDA.JPG "Generic network diagram")  

Generic network diagram.  

---

# Setting up the Raspberry VM

In VMWW install the Raspberry. VMWW = VMware Workstation.

https://gitlab.com/PerPer/networking/-/tree/master/Raspberry

Do updates and upgrades:

$ `sudo apt update`

$ `sudo apt ugrade`

## Install Arduino Sketch

pi@raspberry:~ $ `sudo apt install arduino`  
Reading package lists... Done  
Building dependency tree  
...  

## Start Arduino Sketch

Just to check that installation was ok, open a command prompt/terminal and run:

pi@raspberry:~ $ `sudo arduino`

![Semantic description of image](Arduino_run_from_terminal.JPG "Generic network diagram")  


![Semantic description of image](Arduino_Sketch_GUI.JPG "Generic network diagram")  

Shut down Arduino Sketch

## Put a serial port onto the Raspberry VM

Turn of the Raspberry VM and attatche a serial port to it:

![Semantic description of image](Arduino_add_a_serial_port.JPG "Generic network diagram")  


![Semantic description of image](Arduino_serial_port_settings.JPG "Generic network diagram") 


## Connect the Arduino serial USB to the Raspberry VM

Have the VM turned on but not the Arduino Sketch running. Connect the USB cable to the host lab top. Here the cable is blue:

![Semantic description of image](Arduino_to_host_PC_USB_cable.JPG "Generic network diagram")

The host labtop in tandem with VMWW will ask where to conect the USB device to:

![Semantic description of image](Arduino_usb_serial_first_connect.JPG "Generic network diagram")  

## Run the Arduino connected to the Arduino board

Open a command prompt/terminal and run:

pi@raspberry:~ $ `sudo arduino`

Open Tools and select the ttyUSB0 serial port.

![Semantic description of image](Arduino_choose_USB_serial_port.JPG "Generic network diagram")  


Select the actual Arduino board version:

![Semantic description of image](Arduino_select_the_board.JPG "Generic network diagram")

Run the serial monitor to see if the connection to the Arduino board is working. If no error messages show up all should be fine.

## Run the blinky program

Navigate to File/Example/01 Basisc/Blink and copy paste it in:

![Semantic description of image](Arduino_paste_in_blink.JPG "Generic network diagram")

Click the upload button, and observe how the LED on the board port 18(?) starts to blink.

---

## Blink program

/*  
  Blink  
  Turns on an LED on for half a second, then off for a quarter of a second, repeatedly.  
   
  This example code is in the public domain.  
 */  
   
// Pin 13 has an LED connected on most Arduino boards.  
// give it a name:  
int led = 13;  

// the setup routine runs once when you press reset:  
void setup() {  
  // initialize the digital pin as an output.  
  pinMode(led, OUTPUT);      
}

// the loop routine runs over and over again forever:  
void loop() {  
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)  
  delay(500);               // wait for a second  
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW  
  delay(250);               // wait for a second  
}  

