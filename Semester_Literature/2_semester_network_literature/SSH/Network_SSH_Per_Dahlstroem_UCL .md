---
title: 'Network SSH'
subtitle: 'Linux and Windows'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Network SSH'
---

By Per Dahlstroem pda@ucl.dk

# Audience

The main audience is students at the two year IT Technology education at UCL in Odense Denmark ucl.dk/international

---

# Purpose

The purpose of this document is to provide students with entry level insight into using SSH. To some extend there will be explanations of what takes place behind the scenes, but only to the extend needed to use SSH. The encryption theory is mainly out of scope of this document.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

This  serves as the foundation for describing and demonstrating ssh.

The network consists of three routes R1, R2 and R3. R1 and R2 are interconnected through network 10.10.10.0/24. R2 and R3 are interconnected through network 10.56.16.0/22. The ssh client PC3 and the ssh server PC 4 are both debian Xubuntu Linux machines.

The network was build in VMware Workstation.


![Semantic description of image](ssh_images/SSH_Generic_Network_PDA_V01.JPG "")  

---

# man

In general the man progaram serves as the portal to access the manuals for Linux programs.

* `man ssh`

Display the manual for any program by the man command. E.g. here the manual for the ssh client program.

* Type `q` to exit the manual.

---

# Quick guide to setting up client keys

This is the quick guide part. This quick guide part is followed by a more in dept walk throughs of the mecanics behind setting up ssh keys.

1. Install ssh open server on server: $ `sudo apt install openssh-server`
2. Check status of the ssh server: $ `sudo systemctl status ssh`

---

# Fundamentally two different sets of keys

There are fundamentally two sets of SSH keys in play when logging on to a server.

* server public and private keys. From now on refered to as host keys.
* client public and private keys. From now on refered to as client keys.

Source:

https://security.stackexchange.com/questions/84773/difference-between-public-key-and-host-key-and-security-of-host-key

---

# Running the SSH program on client machine PC3

Without having installed the ssh server program on the server PC4, run the ssh client program on the client machine PC3 to log on to PC4. This will ofcourse fail as there is no running ssh server on PC4.

* per@ssh_client:~$ `ssh per@192.168.13.3` 

ssh is the name of the ssh client program.  

per is the username of the username who will be logged in on the ssh server as.  
 
The IP address 192.168.13.3 is the IP address of the ssh server target computer PC4 that will be logged in to.

 Alternative ssh log in command:

 * per@ssh_client:~$ `ssh 192.168.13.3 -l per`

-l here stands for login.

If a username is not specified, the name of the current user on the client machine will be used. In this case also as user per. This user must beforehand have been created on the server for a succesfull login.

As the ssh server program is not running on the server the connection to it is obviously refused as is the case here:

ssh: connect to host 192.168.13.3 port 22: Connection refused

---

# Set a name in the /etc/hosts file

This section is not ssh specific, but just a convenient setting when working with logging on to different IP adresses. Inn this way device names rather than IP adresses have to be memorized.

The /etc/hosts file holds device names and their IP addresses. The file is used by DNS to resolve domain or device names locally. In this case on the client PC3.

Here an entry for the ssh servers IP address 192.168.13.3 is made in order to be able to use the server name ssh_server instead of having to remember the IP address 192.168.13.3.

![Semantic description of image](ssh_images/SSH_Linux_hosts_file.JPG "")  

Test on the client:

* per@ssh_client:~$ `ping ssh_server$`

Source:
https://www.tecmint.com/setup-local-dns-using-etc-hosts-file-in-linux/

---

# Install a SSH server on PC4 server machine

In order to be able to log on to PC4 by ssh it needs to have a ssh server service running. By default at installation, this service is set to listen on tcp port 22. Install the ssh server program on server PC4:

$ `sudo apt install openssh-server`  

At installation a ECDSA key is generated on the server:  
...  
Creating SSH2 ECDSA key; this may take some time ...  
256 SHA256:+5mpUT5CIZiPaR7aRpEbYHA+/2a8dBNGqHuQHs8aFI0 root@ubuntu (ECDSA)  
...

Check the status of the ssh service:  

* $ `sudo systemctl status ssh`

![Semantic description of image](ssh_images/SSH_systemctl_status_ssh.JPG "") 

If necessary stop and start the server: 

($ `sudo systemctl enable ssh`)  

$ `sudo systemctl stop ssh`  

$ `sudo systemctl start ssh`  

---

# Uninstall a SSH server from PC4 server machine

$ `sudo service ssh stop`

or 

$ `sudo systemctl stop ssh`

Then:

$ `sudo apt --purge remove openssh-server`

or 

$ `sudo apt --purge remove openssh-client openssh-server`

---

# Loging in to ssh server PC4

https://www.vandyke.com/solutions/host_keys/host_keys.pdf

![Semantic description of image](ssh_images/SSH_host_key_pair_overview.JPG "")


Loging in to a ssh server using server host private and public key pair will in most cases be referred to as logging on to ssh server using the servers host keys.

Run the ssh program on the client machine PC3 to ssh connect to the ssh server PC4:

per@ssh_client:~$ `ssh per@ssh_server`  

If this is the first time ever logging on to the server the client will have to accept the servers host public key.

This is also called: ECDSA key fingerprint is SHA256:+5mpUT5CI...

If accepted this host public key will be stored on the client PC3 along with the servers name in the file called `known hosts` in the current client users .ssh directory. In other words, this is the file holding the public keys to the servers that this user knows and has logged in to earlier.

The authenticity of host 'ssh_server (192.168.13.3)' can't be established.  
ECDSA key fingerprint is SHA256:+5mpUT5CIZiPaR7aRpEbYHA+/2a8dBNGqHuQHs8aFI0.  
Are you sure you want to continue connecting (yes/no)?  

By typing `yes` the servers host public key is thus copied into the clients ~/.ssh/known hosts file, and the client will from now on be able to log on to the server, but will always have to use the password for this user.

![Semantic description of image](ssh_images/SSH_host_key_transfer.JPG "") 

 The server name and IP address and key in the known_hosts fill on the client are all hashed and thus not humanly recognisable in the file for security reasons.

![Semantic description of image](ssh_images/SSH_client_know_hosts_file.JPG "")

Each user on a client machine has her own known_hosts file.

It will be necessary to type the password for the user that is being logged in as on the server :

<p style="font-family: courier, new; font-size:12pt; font-style:bold">
per@ssh_server's password:  <br>

Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-74-generic x86_64)  <br>
...

To exit the server:

per@ssh_server:~$ `exit`  
logout  
Connection to ssh_server closed.  
per@ssh_client:~$  
</p> 

## Host public key renewal on the client

On the client, delete the entry in the known_hosts file. When logging in to the server from the client the afore shown process will be repeated and the servers host public key alias key fingerprint will again be copied into the known_hosts file on the client.

## Create another user bob on the client machine PC3:

per@ssh_client:~$ `sudo adduser bob`

As bob is not yet created as a user on the ssh server PC4, he can not log in.

![Semantic description of image](ssh_images/SSH_bob_permission_denied.JPG "")



But he can log in to pers "account" if he knows pers password!

bob@ssh_client:~$ `ssh per@192.168.13.3` 

per@192.168.13.3's password:  
Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-74-generic x86_64)  
...

---

# Password free logging in to ssh server using client private and public key pair

Using only keys is more secure than using keys and a password. In other words, using keys can be made password free.

It is required to create a key pair on the client consisting of a client private and a public key. The client will hold the private and the public key and the server will be send and hold a copy of the public key.

Here the key pair will be created on the client and the client will thereafter copy the public key to the server.

Run the ssh-keygen program:

per@ssh_client:~$ `ssh-keygen -t rsa`  

![Semantic description of image](ssh_images/SSH_key_pair.JPG "") 



Protect the private key with a password. (perpass) This is not the password to log on to an user account on a server.

![Semantic description of image](ssh_images/SSH_key_pair_fingerprint.JPG "") 

The new private and public keys were placed in the ~/.ssh folder and named id_rsa and id_rsa.pub.

![Semantic description of image](ssh_images/SSH_keys_in_folder_graphics.JPG "")

## Copy the public key to the ssh server.

**The same user must beforehand have been created on both the server and the client.**  
**I.e. create the same user on the server, in this case user: per**

Then copy the public key to user per on the ssh server:

per@ssh_client:~/.ssh$ `ssh-copy-id per@ssh_server`  
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed  
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys  
per@ssh_server's password:  

Number of key(s) added: 1  

Now try logging into the server machine, with: "ssh 'per@ssh_server'" and check to make sure that only the key(s) you wanted were added.  

per@ssh_client:~/.ssh$  

Note: If this was the first time ever contacting the server, the client user will be asked to install the host public key, as described in the previous sections.

The public key is now copied to the servers /home/per/.ssh/authorized_keys file:

![Semantic description of image](ssh_images/SSH_public_key_on_server.JPG "")


Logging in from PC3 to the server PC4 can now be done without typing the user password.  

per@ssh_client:~/.ssh$ `ssh per@ssh_server`

![Semantic description of image](ssh_images/SSH_password_free_login.JPG "")

It can be verified that the authorized_keys file has been created and populated on the server. 

![Semantic description of image](ssh_images/SSH_autorized_keys_file.JPG "")

Inspect the public key(s) is in that /home/per/.ssh/authorized_keys file.

![Semantic description of image](ssh_images/SSH_server_authorized_keys_file.JPG "")

Deleting the key will "deauthorize" the client.

# Can root user log in to the server

What if super user or root user tries to log in to the root account on the server?

per@ssh_client:~/.ssh$ `sudo -i`  
[sudo] password for per:  
root@ssh_client:~# `ssh root@ssh_server`  

This will create and populate the /root/.ssh directorys known_hosts file on the client machine PC3.

But if the root password is not know, the server will not let the client root user in to the root account on the server.  

# Each user on the client machine has her own set of keys

Each user has her own client public and private keys on her client machine and each user has her own public key on the server machine. Here ilustrated with users per and bob:

![Semantic description of image](ssh_images/SSH_keys_for_each_user.JPG "")

Each user has her own known_hosts file on the client machine. I believe that all clients get the same host public key from the server, but it is not important I believe.

![Semantic description of image](ssh_images/SSH_all_users_known_hosts_file.JPG "")

The keys can be deleted by going into the respective files and delete them. Of course this should be done with causion and a good understanding of what keys can or should be deletede and not deleted. Deleting a key means that there no longer can be logged in using that key, and a new key generation or copying or both, process, must be run through again to regain access.

---

# Using keys with Putty for password free login

Putty uses its own format for the private key on the client. This means that the id_rsa private key generated earlier with keygen has to be copied to a copy that has the converted Putty .ppk format. (.ppk = private putty key)

Install and use the puttygen program to do this. 

per@ssh_client:~/.ssh$ `sudo apt install putty-tools`

Navigate to the /home/per/.ssh folder and run the program:

per@ssh_client:~/.ssh$ `puttygen id_rsa -o id_rsa.ppk`

![Semantic description of image](ssh_images/SSH_Putty_ppk_private_key.JPG "")

The private key in Putty format is listed as id_rsa.ppk. I believe that any name can be used(?).

![Semantic description of image](ssh_images/SSH_Putty_ppk_private_key_file.JPG "")

## Setting up the parameters in Putty

Set the IP address of the server.  
Hint: When all setting for this connection are done it is time saving to save the setup with a descriptive name for later recall and reuse.

![Semantic description of image](ssh_images/SSH_putty_authentication_session.JPG "")

In the Auth section, enter the full path to the newly created id_rsa.pkk private key here on the client:

![Semantic description of image](ssh_images/SSH_putty_authentication_private_key.JPG "")

Maybe also set the font to a nice size and the colors to something pleasant:

![Semantic description of image](ssh_images/SSH_putty_font_settings.JPG "")

And finally it is possible to log in to the server from the client with just the user or login name:

![Semantic description of image](ssh_images/SSH_putty_login_with_private_key.JPG "")

To exit the session type: exit

It should be possible to enter both user name and IP address in Putty session, but I could not get that to work:

`per@192.168.13.3`

Fatal error in name resolution.

Solved:

Source:  
https://websiteforstudents.com/setup-passwordless-login-putty-connect-ubuntu-17-04-17-10/


---

# Using ssh to secure copy files with scp

SCP stands for Secure Copy Protocol.  

The scp secure copy program uses the set up ssh keys to authenticate and encrypt the transmission of files from the client to the server and vice versa.

Just to have something experimental to copy, a file named my_dir.txt is cretade in the following way. Note that any file can be copied over scp.

per@ssh_client:~$ `ls -all | grep drwx > my_dir.txt`

To copy this file to the server run the scp program. Note that the ":" is important.

per@ssh_client:~$ `scp my_dir.txt per@ssh_server:`

The file is now in pers home directory on the ssh server.

![Semantic description of image](ssh_images/SCP_file_to_serverh.JPG "")

If the file shoul end up in an other directory on the server, specify the path to that directory:

per@ssh_client:~$ `scp my_dir.txt per@ssh_server:/home/per/Delete_Me`

This will copy the file into the /home/per/Delete_Me directory on the server. If the directory dose not exist the my_dir.txt will be copied and renamed to Delete_Me in the servers /home/per directory!

### scp from server to client

scp also works to pull or copy a file from the server to the client. Note the "." at the end which means, copy the file to the current cirectory on the client.

per@ssh_client:~$ `scp per@ssh_server:/home/per/Delete_Me/my_dir.txt .`

An alternative directory to copy the file into can be specified.

per@ssh_client:~$ `scp per@ssh_server:/home/per/Delete_Me/my_dir.txt /home/per/Temp`

---

# Using ssh to secure copy files with SFTP from commandline

Open an interactive session on the client with the server

per@ssh_client:~$ `sftp per@ssh_server`

![Semantic description of image](ssh_images/SFTP_interactive_session.JPG "")

---

# Using ssh to secure copy files with SFTP and FileZilla

A powerfull file transfer tool using ssh is FileZilla, which runs on both Linux, Windows and OSX.

Install FileZilla

per@ssh_client:~$ `sudo apt get install filezilla`

Start FileZilla

per@ssh_client:~$ `filezilla`

In the top, enter the credentials for the server that should be logged in to and press Quickconnect.

![Semantic description of image](ssh_images/SFTP_FileZilla_log_in.JPG "")

And voila. It is now a matter of drag and drop to copy files in both directions.

![Semantic description of image](ssh_images/SFTP_FileZilla_logged_in.JPG "")

And here I discovered: Mousepad which is the editor FileZilla opens up when an editor is needed! A lot easier to use than nano when e.g. a key has to be copied.

![Semantic description of image](ssh_images/SFTP_Mousepad.JPG "")

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKJqt6oyQ6tKzdjywYZCJB5H4nqS/aeo+2zic9XC46jbZrRtSA1PZ6eCKoFX8KW3DPzoVCb8tCDG2Kif0pg1LKiVGD5Ed2/orX5T722EEHtJ7khQ6+Jj+QoSHBccAwaLv6e+R2Ul/eenQs4FgrZ6durs09ol+XBXT2DZH/v/R5koKmLSs6ymD5Icw8DVqSO3F/kJsHB9tMyDfyxOiSWHcPTah6EaooOAJAydRQdrePEXaKcOhkLfMIGwFfRl0qbGy/0OTNwvLwIYQUk0cLejLS0W+WaMbSvmHsxDCtuEcAeAy0Y0/7EIhNyAczQPlBtIb/qbFtA9TkxIG5w1kO8EJ3 /home/per/.ssh/id_rsa

---

# Create an SSH tunnel

TBD

---

# ssh to junos
![Semantic description of image](ssh_images/SSH_host_key_pair_overview.JPG "")

First a user must be created on the junos router R2. The user used and created here continues to be me, per.

[edit system login]  
root@vSRX-NAT# set user per uid 2020 class super-user  
  
[edit system login]  
root@vSRX-NAT# show  
user per {  
    uid 2020;  
    class super-user; 
}  

An authentication method has not yet been set. But here below the options are listed.

[edit system login]  
root@vSRX-R2# `set user per authentication ?`  
Possible completions:  
apply-groups         Groups from which to inherit configuration data  
apply-groups-except  Don't inherit configuration data from these groups  
encrypted-password   Encrypted password string  
load-key-file        File (URL) containing one or more ssh keys  
plain-text-password  Prompt for plain text password (autoencrypted)  
ssh-dsa              Secure shell (ssh) DSA public key string  
ssh-ecdsa            Secure shell (ssh) ECDSA public key string  
ssh-ed25519          Secure shell (ssh) ED25519 public key string  
ssh-rsa              Secure shell (ssh) RSA public key string  

For convenience the R2 router name can be put in the /etc/hosts file.

 ![Semantic description of image](ssh_images/Junos_R2_in_hosts_file.JPG "")

Trying to login from client PC3 to router R2 is not possible as there is not set a password for user per:

![Semantic description of image](ssh_images/Junos_R2_login_not_possible.JPG "")

But the servers, in this case the router R2 is the server, public host key was copied to client PC3 to user pers /home/per/.ssh/known_hosts file, as the messages above tries to tell.

Moving over to the router and set a password:

[edit system login]  
root@vSRX-R2# `set user per authentication plain-text-password`  
New password:  
Retype new password:  

Dont forget to commit. And now logging in on R2 with ssh with the new user per R2 password is permitted:

![Semantic description of image](ssh_images/Junos_R2_login_is_possible.JPG "")

To exit the router:

per@vSRX-R2> `exit`  

The total loggin configuration for user per on router R2 now shows that authentication is with an encrypted password.

![Semantic description of image](ssh_images/Junos_R2_login_password_config.JPG "")

## But here the intention is to use client private and public key to log in password free.

Here is first a description on a rather simple but efficient way to copy the client PC3 public key to router R2. Open the id_ras.pub file with an editor and copy the entire public key string. Mousepad seems to be easier to use for this copying than e.g. nano.

![Semantic description of image](ssh_images/Junos_copy_client_pub_key.JPG "")

On the router R2 navigate to edit system login user per authentication ssh-rsa and paste the key in. It seems that pasting is done by a right click on the mouse.  

The key string must be started with a double quotationmark and also ended with a double quotation mark.

[edit system login]  
root@vSRX-R2# `set user per authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EA...`

Note the double quotationmarks and make sure to get the entire string copied.

![Semantic description of image](ssh_images/Junos_copy_paste_pub_file.JPG "")


Commit and now it is posible to log in to R2 without password from client PC3:

![Semantic description of image](ssh_images/Junos_logged_in_to_R2.JPG "")

## Copy id_rsa.pub to junos with scp

It is possible to copy the id_rsa.pub to the router by using scp. If the user is only having password authentication set on the router, the routes will prompt for the users password:

![Semantic description of image](ssh_images/Junos_scp_client_pub_key.JPG "")

If the user is already having a client public key on the router for authentication, the file transfer will take place without password prompt:

![Semantic description of image](ssh_images/SCP_id_rsa_pub_file_to_serverh.JPG "")

And then on the router R2 set the authentication to load-key-file and the path to /cf/var/home/per and the filename to id_rsa.pub:

[edit system login]  
per@vSRX-R2# `set user per authentication load-key-file /cf/var/home/per/.ssh/id_rsa.pub `

Commit and check that the authentication is now also set top ssh-rsa. 

![Semantic description of image](ssh_images/Junos_copy_paste_pub_file.JPG "")

And of course also test that the client can log in password free.

![Semantic description of image](ssh_images/Junos_logged_in_to_R2.JPG "")

---

## Monitor the ssh process

$ `tail -f -n 500 /var/log/auth.log | grep 'sshd'`

## Pull the public key from the client to the router

From the router the public key can also be pulled or copied with ssh from the client to the router.

First openss-server has to be instaleld on the client for the router to be able ssh login and to fetch the clients public key in a client directory. In essens the client is now also configured to be both a ssh client and a ssh server.

Do an openss-server installation without any tweaking as described earlier in this document.

On the router R2, set the authentication load-key-file pointing at the clients IP address and the clients directory path and file to fetch. In this case the id_rsa.pub key file:

![Semantic description of image](ssh_images/Junos_load-key-file.JPG "")

Commit and password free login is again a reality:

![Semantic description of image](ssh_images/Junos_logged_in_to_R2.JPG "")  

Source:
https://www.juniper.net/documentation/en_US/junos/topics/topic-map/junos-os-user-accounts.html

## Filezilla to junos

Is is also possible to use Filezilla on PC3 ssh sftp client, to copy files to and from a junos box. In this case to and from router R2. Enter the credentials in the top and Quickconnect. The port number is 22 as ftp is run over ssh and becomes sftp.

![Semantic description of image](ssh_images/Junos_filezilla.JPG "")

The client public key file can be copied with filezilla to the junos box:

![Semantic description of image](ssh_images/Junos_filezilla_copy_local_pub__key_file.JPG "")

And then on the junos box the client public key can be loaded from local .ssh directory:

![Semantic description of image](ssh_images/Junos_load-loca_key_file.JPG "")

Note how the client public key is copiend into the configuration. This means that the id_rsa.pub can be removed from the /cf/var/home/per/.ssh/ directory. It can also be left there of course and then overwritten with a new one in case of renewal.

---





# Make Junos auto archive to a ssh server on commit

Junos can be configured to transmit its active configuration to a ssh server every time the user issues the commit command on a Junos box.

This automatic transmission requires that password free login from the Junos box to the ssh server has been set up on the Junos box and the ssh server. See previous section on how to to do that.

In this way the Junoas box becomes the ssh client.

In this example PC3 is now turned into also being a ssh server.

Configure login: (And no, the keys are NOT my real keys. Feel free to test that.)

Configuring login means creating a user, in this case user "per", and copyining the clinet user pers public client key to the Junos box. When done so the login configuration should be similar to what is shown here:

    login {
        user per {
            uid 2020;
            class super-user;
            authentication {
                encrypted-password "$1$MqEo7eB1$baEcWYIkqTKrfVKWgRUqZ0"; ## Perpass
                /* Linux ssh client public key. */
                ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAAABAQDKJqt6oyQ6tKzdjywYZCJB5H4nqS/aeo+2zic9XC46jbZrRtSA1PZ6eCKoFX8KW3DPzoV                                                         Cb8tCDG2Kif0pg1LKiVGD5Ed2/orX5T722EEHtJ7khQ6+Jj+QoSHBccAwaLv6e+R2Ul/eenQs4FgrZ6durs09ol+XBXT2DZH/v/R5koKmLSs6ymD5Icw8DVqSO3F/kJsHB9t                                                         MyDfyxOiSWHcPTah6EaooOAJAydkLfMIGwFfRl0qbGy/0OTNwvLwIYQUk0cLejLS0W+WaMbSvmHsxDCtuEcAeAy0Y0/7EIhNyAczQPlBtIb/qbFtA9TkxIG                                                         5w1kO8EJ3 per@ssh_client"; ## SECRET-DATA
                /* Windows 10 public key also used for GitLab. */
                ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAM491D9Bpd56IWt6rCf3GoE440qmdd66v4tRHhVemSPuqV2IzPW                                                         UHQ+JAghrbHjUxpeNk95skN/4jzjvu86S6VrJfaWB/R6Qg8tJQl+Q6/L/7DxfnHQHJpIa3UJd2IF9HaWbfgtXkeIT3/WwI1aGsuzWGLvBaBv+OQLMdu3Bxh40HL7WeGacsqd                                                         hmEKaR5whRjy9OttbznMIxITex/OdlnpXciLz5o6ZbEg3VOjeAr4ttG73Pn28huEQtfSLECGwF/USsnwAtXiK5pQqi7TlEASDQBA+PyyPOSGgiKH/mp2bGqvOJJqCYuX/                                                         8tdW6c96b pda@EAL-01189"; ## SECRET-DATA
            }
        }
    }

When the user and corresponding keys have been set up it is now time to configure the automatic configuration on commit archiving under the system settings configure archival:

    archival {
        configuration {
            transfer-on-commit;
            archive-sites {
                "scp://per@192.168.12.2:/home/per/Junos_Configs" password "$9$1r1ISeLX-bYo8X7VY2UDCtu"; ## SECRET-DATA
            }
        }
    }

On the next commit the configuration will show up in the directory on the ssh server. In this case in the /home/per/Junos_Configs directory.

![Semantic description of image](ssh_images/Junos_configs_on_server.JPG "")

The configuration file that landed on the ssh server is named something like: vSRX_R2_juniper.conf.gz_20190122_065016.gz. The gz file extension indicates that the file is zipped. I.e. when it is openede in a text editor is is listed in gyberish:

![Semantic description of image](ssh_images/Junos_config_gz_file_from_R2.JPG "")

Use gunzip to unzip the file to see the configuration in the Junos configuration json like format:. NOTE how .gz is added to the end of the file before running gunzip in order to signal that is a zipped file. Use the linux `cp` or `mv` command for this.

per@ubuntu:~/scpTestFolder$ `gunzip vSRX_1_juniper.conf.gz_20190122_065016.gz`

Now the file content can be viewed and edited in the Junos configuration json like format:

per@ubuntu:~/scpTestFolder$ `nano vSRX_1_juniper.conf.gz_20190122_065016`

![Semantic description of image](ssh_images/Junos_config_gunzip_file_from_R2.JPG "")

## Load the configuration back onto Junos

It is only the unzipped configuration file that can be loaded by Junos. Or in other words, the file content must be a valid configuration in the correct junos json like syntax.

On the Junos run the load command:

[edit]
per@vSRX-R2# `load override per@192.168.12.2:/home/per/Junos_Configs/vSRX-R2_juniper.conf.gz_20200126_210540`

A commit is needed on the Junos in order to make the just loaded configuration the active configuration.

Source:  
https://www.juniper.net/documentation/en_US/junos10.0/information-products/topic-collections/config-guide-system-basics/junos-software-system-management-router-configuration-archiving.html

---

# Tweaking the debian ssh server

There are settings for the ssh server configuration that can be usefull.

Open the sshd_config file on the server.

To change the listening port uncomment and set the desired port number here:

#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

To put in a personalized banner, change:

#Banner none

Change none to a path to a banner file and create that banner. Se example below.

Banner /home/per/ssh_banner/banner

per@ssh_server:~/ssh_banner$ `nano banner`

Restart the ssh server or service:

per@ssh_server:~/ssh_banner$ `service sshd restart`

When logging in on the client the banner will be displayed before the usual welcome text:

![Semantic description of image](ssh_images/SSH_server_banner.JPG "")
